package template_method

import (
	"fmt"
	"math/rand"
	"strings"
	"testing"
	"time"
)

func TestGenAndSendOTP(t *testing.T) {

	cityListStr := ",5,"
	index := strings.Index(",1,2,3,", cityListStr)
	fmt.Println(index)

	rand.Seed(time.Now().UnixNano())

	randomNumber := rand.Intn(3-1) + 1 // 生成范围从0到9999999999之间的随机数

	fmt.Println("随机数字:", randomNumber)

}
