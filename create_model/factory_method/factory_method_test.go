package factory_method

import (
	"designmode/create_model/factory_method/operator_factory"
	"fmt"
	"testing"
)

func compute(factory operator_factory.OperatorFactory, a, b int) int {
	op := factory.Create()
	op.SetA(a)
	op.SetB(b)

	return op.Result()
}

func TestMinusOperator(t *testing.T) {
	var factory operator_factory.OperatorFactory

	factory = operator_factory.PlusOperatorFactory{}
	rs := compute(factory, 1, 2)
	fmt.Println(rs)

	factory = operator_factory.MinusOperatorFactory{}
	rs = compute(factory, 4, 5)
	fmt.Println(rs)

	factory = operator_factory.MultiplicationOperatorFactory{}
	rs = compute(factory, 4, 5)
	fmt.Println(rs)
}
