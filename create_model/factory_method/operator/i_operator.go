package operator

type IOperator interface {
	SetA(int)
	SetB(int)
	Result() int
}
