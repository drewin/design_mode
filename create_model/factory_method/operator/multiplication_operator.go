package operator

type MultiplicationOperator struct {
	*OperatorBase
}

func (m *MultiplicationOperator) Result() int {
	return m.a * m.b
}
