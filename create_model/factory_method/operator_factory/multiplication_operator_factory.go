package operator_factory

import "designmode/create_model/factory_method/operator"

// PlusOperatorFactory 是 PlusOperator 的工厂类, 该结构体实现了OperatorFactory接口
type MultiplicationOperatorFactory struct{}

func (MultiplicationOperatorFactory) Create() operator.IOperator {
	return &operator.MultiplicationOperator{
		OperatorBase: &operator.OperatorBase{},
	}
}
