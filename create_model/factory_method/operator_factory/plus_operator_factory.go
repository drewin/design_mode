package operator_factory

import "designmode/create_model/factory_method/operator"

// PlusOperatorFactory 是 PlusOperator 的工厂类, 该结构体实现了OperatorFactory接口
type PlusOperatorFactory struct{}

func (PlusOperatorFactory) Create() operator.IOperator {
	return &operator.PlusOperator{
		OperatorBase: &operator.OperatorBase{},
	}
}
