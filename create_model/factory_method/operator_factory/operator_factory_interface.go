package operator_factory

import "designmode/create_model/factory_method/operator"

// go中不存在继承、所以用匿名组合来实现

type OperatorFactory interface {
	Create() operator.IOperator
}
