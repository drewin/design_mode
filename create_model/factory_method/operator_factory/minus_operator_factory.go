package operator_factory

import "designmode/create_model/factory_method/operator"

// MinusOperatorFactory 是 MinusOperator 的工厂类, 该结构体实现了OperatorFactory接口
type MinusOperatorFactory struct{}

func (MinusOperatorFactory) Create() operator.IOperator {
	return &operator.MinusOperator{
		OperatorBase: &operator.OperatorBase{},
	}
}
