package factory

import (
	"designmode/create_model/abstract_factory/shirt"
	"designmode/create_model/abstract_factory/shoe"
)

type Nike struct {
}

func (n *Nike) MakeShoe() shoe.IShoe {
	return &shoe.NikeShoe{
		Shoe: shoe.Shoe{
			Logo: "nike",
			Size: 14,
		},
	}
}

func (n *Nike) MakeShirt() shirt.IShirt {
	return &shirt.NikeShirt{
		Shirt: shirt.Shirt{
			Logo: "nike",
			Size: 14,
		},
	}
}
