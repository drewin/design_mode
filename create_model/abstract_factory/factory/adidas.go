package factory

import (
	"designmode/create_model/abstract_factory/shirt"
	"designmode/create_model/abstract_factory/shoe"
)

type Adidas struct {
}

func (a *Adidas) MakeShoe() shoe.IShoe {
	return &shoe.AdidasShoe{
		Shoe: shoe.Shoe{
			Logo: "adidas",
			Size: 14,
		},
	}
}

func (a *Adidas) MakeShirt() shirt.IShirt {
	return &shirt.AdidasShirt{
		Shirt: shirt.Shirt{
			Logo: "adidas",
			Size: 14,
		},
	}
}
