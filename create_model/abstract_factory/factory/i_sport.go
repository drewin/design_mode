package factory

import (
	"designmode/create_model/abstract_factory/shirt"
	"designmode/create_model/abstract_factory/shoe"
	"fmt"
)

type ISportsFactory interface {
	MakeShoe() shoe.IShoe
	MakeShirt() shirt.IShirt
}

func GetSportsFactory(brand string) (ISportsFactory, error) {
	if brand == "adidas" {
		return &Adidas{}, nil
	}

	if brand == "nike" {
		return &Nike{}, nil
	}

	return nil, fmt.Errorf("Wrong brand type passed")
}
