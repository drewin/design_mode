package abstract_factory

import (
	"designmode/create_model/abstract_factory/factory"
	"designmode/create_model/abstract_factory/shirt"
	"designmode/create_model/abstract_factory/shoe"
	"fmt"
	"testing"
)

func TestISportsFactory(t *testing.T) {
	adidasFactory, _ := factory.GetSportsFactory("adidas")
	nikeFactory, _ := factory.GetSportsFactory("nike")

	adidasShoe := adidasFactory.MakeShoe()
	adidasShirt := adidasFactory.MakeShirt()

	nikeShoe := nikeFactory.MakeShoe()
	nikeShirt := nikeFactory.MakeShirt()

	printShoeDetails(adidasShoe)
	printShoeDetails(nikeShoe)

	printShirtDetails(adidasShirt)
	printShirtDetails(nikeShirt)
}

func printShoeDetails(s shoe.IShoe) {
	fmt.Printf("Logo: %s", s.GetLogo())
	fmt.Println()
	fmt.Printf("Size: %d", s.GetSize())
	fmt.Println()
}

func printShirtDetails(s shirt.IShirt) {
	fmt.Printf("Logo: %s", s.GetLogo())
	fmt.Println()
	fmt.Printf("Size: %d", s.GetSize())
	fmt.Println()
}
