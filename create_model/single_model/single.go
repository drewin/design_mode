package single_model

import (
	"fmt"
	"sync"
)

var lock = &sync.Mutex{}
var once sync.Once

type single struct {
}

var singleInstance *single

func getInstance() *single {
	if singleInstance == nil {
		lock.Lock()
		defer lock.Unlock()

		if singleInstance == nil {
			fmt.Println("Creating single instance now.")
			singleInstance = new(single)
		} else {
			fmt.Println("Single instance already created2.")
		}

	} else {
		fmt.Println("Single instance already created1.")
	}

	return singleInstance

}

func getInstance2() *single {
	if singleInstance == nil {
		once.Do(func() {
			singleInstance = new(single)
			fmt.Println("Creating single instance now.")
		})

	} else {
		fmt.Println("Single instance already created.")
	}

	return singleInstance
}
