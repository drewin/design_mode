package simple_factory

import "fmt"

type IAPI interface {
	Say(name string) string
}

func NewAPI(t int) IAPI {
	if t == 1 {
		return &hiAPI{}
	}

	if t == 2 {
		return &helloAPI{}
	}

	return nil
}

type hiAPI struct{}

// Say hi to name
func (*hiAPI) Say(name string) string {
	return fmt.Sprintf("Hi, %s", name)
}

// HelloAPI is another API implement
type helloAPI struct{}

// Say hello to name
func (*helloAPI) Say(name string) string {
	return fmt.Sprintf("Hello, %s", name)
}
