package simple_factory

import (
	"fmt"
	"testing"
)

func TestNewAPI(t *testing.T) {
	say := NewAPI(1).Say("world")
	fmt.Println(say)
}
